$(function () {

  let $slider = $('.cards');

  let $nextBtns = $('.next');

  let slickOptions = {
    dots: true,
    arrows: false,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    variableWidth: true,
    mobileFirst: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: "unslick"
      }
    ]
  };

  $nextBtns.on('click', changeSlides);

  $(window).resize(function () {
    const isMobile = window.innerWidth < 1024;

    if (isMobile) {

      if (!$slider.hasClass('slick-initialized')) {
        $slider.slick(slickOptions);
      }
    }
  });

  function changeSlides() {

    $('.cards').slick('slickNext');
  }
})